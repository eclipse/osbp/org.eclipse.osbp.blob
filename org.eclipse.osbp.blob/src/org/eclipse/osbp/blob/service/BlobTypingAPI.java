/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * Jose Dominguez (Compex Systemhaus GmbH) - ongoing development 
 */
package org.eclipse.osbp.blob.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.eclipse.osbp.blob.entities.BlobTyping;
import org.eclipse.osbp.blob.entities.ContentType;
import org.eclipse.osbp.blob.entities.MimeType;
import org.eclipse.osbp.blob.entities.NormalizerResolution;
import org.eclipse.osbp.ui.api.customfields.IBlobTyping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BlobTypingAPI is a API that provides predefined blob typing data that are
 * required to be able to work with Blob objects.
 * 
 * The predefined data stored in a blob typing xml file will be read out and
 * will be filled into the corresponding blob typing java objects via JAXB.
 * 
 * @author dominguez
 * 
 */
public class BlobTypingAPI implements IBlobTyping {

	private static final String DEFAULT_BLOBTYPING = "platform:/plugin/org.eclipse.osbp.blob/org/eclipse/osbp/blob/BlobTyping.xml";
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BlobTypingAPI.class);
	private BlobTyping blobTyping;

	public BlobTypingAPI() {
	}

	/**
	 * Constructor in case of a different path to the input xml file.
	 * 
	 * !!! This is also the only usable constructor for JUnit test because the
	 * above OSGI-path to the input xml file is not working for simple JUnit
	 * tests!!!
	 * 
	 * @param xmlFile
	 */
	public BlobTypingAPI(File xmlFile) {
		try {
			blobTyping = readBlob(xmlFile);
		} catch (JAXBException e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionDetails = sw.toString();
			LOGGER.error("{}", exceptionDetails);
		}
	}

	/**
	 * Method that reads the blob typing xml file and fill the 'BlobTyping'
	 * object via JAXB with all the receiving data from the xml input file.
	 * 
	 * @param xmlFile
	 * @return the {@link BlobTyping} object filled with all the defined data
	 *         from the blob typing xml file.
	 * @throws JAXBException
	 */
	private BlobTyping readBlob(File xmlFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(BlobTyping.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (BlobTyping) jaxbUnmarshaller.unmarshal(xmlFile);
	}

	/**
	 * Method that reads the blob typing xml file and fill the 'BlobTyping'
	 * object via JAXB with all the receiving data from the xml input file.
	 * 
	 * @param xmlFile
	 * @return the {@link BlobTyping} object filled with all the defined data
	 *         from the blob typing xml file.
	 * @throws JAXBException
	 */
	private BlobTyping readBlob(InputStream xmlFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(BlobTyping.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (BlobTyping) jaxbUnmarshaller.unmarshal(xmlFile);
	}

	/**
	 * Provides the defined mime type version.
	 * 
	 * @return the defined mime type version
	 * @see MimeType
	 */
	@Override
	public String getMimeTypeVersion() {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null)
			return lblobTyping.getMimeType().getMimeVersion();
		else
			return null;
	}

	/**
	 * Provides the defined content transfer encoding of the mime type.
	 * 
	 * @return the defined content transfer encoding
	 * @see MimeType
	 */
	@Override
	public String getMimeTypeContentTransferEncoding() {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null)
			return lblobTyping.getMimeType().getContentTransferEncoding();
		else
			return null;
	}

	/**
	 * Provides a list of all the defined content type objects of the mime type
	 * object.
	 * 
	 * @return a list of all the defined {@link ContentType}
	 */
	public List<ContentType> getMimeTypeContentTypeList() {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null)
			return lblobTyping.getMimeType().getContentTypeList();
		else
			return new ArrayList<>(); // SONAR says "return an empty list
										// instead of null"
	}

	/**
	 * Provides a list of all the defined content type names of the mime type
	 * object.
	 * 
	 * @return a list of all the defined content type names
	 * @see ContentType
	 */
	@Override
	public List<String> getMimeTypeContentTypeNames() {
		List<String> mimeTypes = new ArrayList<>();
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			for (ContentType contentType : lblobTyping.getMimeType().getContentTypeList()) {
				mimeTypes.add(contentType.getType());
			}
		}
		return mimeTypes;
	}

	/**
	 * Provides the content type id for an individual position within the list
	 * of all the defined content type objects of the mime type object.
	 * 
	 * @param index
	 * @return the content type id or '-1' if not found
	 * @see ContentType
	 */
	@Override
	public int getMimeTypeContentTypeIdByListIndex(int index) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			int length = lblobTyping.getMimeType().getContentTypeList().size();
			if (index < length) {
				return lblobTyping.getMimeType().getContentTypeList().get(index).getId();
			}
		}
		return -1;
	}

	/**
	 * Provides the content type name for an individual position within the list
	 * of all the defined content type objects of the mime type object.
	 * 
	 * @param index
	 * @return the content type name as {@link String} or ""(blank) if not found
	 * @see ContentType
	 */
	@Override
	public String getMimeTypeContentTypeByListIndex(int index) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			int length = lblobTyping.getMimeType().getContentTypeList().size();
			if (index < length) {
				return lblobTyping.getMimeType().getContentTypeList().get(index).getType();
			}
		}
		return "";
	}

	/**
	 * Provides the content type name for a specific content type id.
	 * 
	 * @param contentTypeId
	 * @return the content type name as {@link String} or ""(blank) if not found
	 * @see ContentType
	 */
	@Override
	public String getMimeTypeContentTypeById(int contentTypeId) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			for (ContentType contentType : lblobTyping.getMimeType().getContentTypeList()) {
				if (contentTypeId == contentType.getId()) {
					return contentType.getType();
				}
			}
		}
		return "";
	}

	/**
	 * Provides the content type id for a specific content type name.
	 * 
	 * @param contentTypeStr
	 * @return the content type id or '-1' if not found
	 * @see ContentType
	 */
	@Override
	public int getMimeTypeContentTypeId(String contentTypeStr) {
		String type = contentTypeStr;
		if("image/jpg".contentEquals(contentTypeStr)) {
			type = "image/jpeg";
		}
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			for (ContentType contentType : lblobTyping.getMimeType().getContentTypeList()) {
				if (type.equals(contentType.getType())) {
					return contentType.getId();
				}
			}
		}
		return -1;
	}

	/**
	 * Provides the defined list of normalizer resolution objects.
	 * 
	 * @return list of {@link NormalizerResolution}
	 */
	public List<NormalizerResolution> getNormalizer() {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			return lblobTyping.getNormalizer();
		}
		return new ArrayList<>();
	}

	/**
	 * Provides the defined default normalizer resolution id.
	 * 
	 * @return default normalizer resolution id
	 */
	@Override
	public int getNormalizerDefaultResolutionId() {
		return getNormalizerResolutionIdByListIndex(0);
	}

	/**
	 * Provides the normalizer resolution id for an individual position within
	 * the list of all the defined resolution objects of the normalizer object.
	 * 
	 * @param index
	 * @return the normalizer resolution id or '-1' if not found
	 */
	@Override
	public int getNormalizerResolutionIdByListIndex(int index) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			int length = lblobTyping.getNormalizer().size();
			if (index < length) {
				return lblobTyping.getNormalizer().get(index).getId();
			}
		}
		return -1;
	}

	/**
	 * Provides the normalizer resolution name for an individual position within
	 * the list of all the defined resolution objects of the normalizer object.
	 * 
	 * @param index
	 * @return the normalizer resolution name as {@link String} or ""(blank) if
	 *         not found
	 */
	@Override
	public String getNormalizerResolutionNameByListIndex(int index) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			int length = lblobTyping.getNormalizer().size();
			if (index < length) {
				return lblobTyping.getNormalizer().get(index).getName();
			}
		}
		return "";
	}

	/**
	 * Provides the normalizer resolution for an individual position within the
	 * list of all the defined resolution objects of the normalizer object.
	 * 
	 * @param index
	 * @return the normalizer resolution as {@link String} or ""(blank) if not
	 *         found
	 */
	@Override
	public String getNormalizerResolutionByListIndex(int index) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			int length = lblobTyping.getNormalizer().size();
			if (index < length) {
				return lblobTyping.getNormalizer().get(index).getResolution();
			}
		}
		return "";
	}

	/**
	 * Provides the normalizer resolution id for a specific normalizer
	 * resolution.
	 * 
	 * @param resolutionStr
	 * @return the normalizer resolution id or '-1' if not found
	 */
	@Override
	public int getNormalizerResolutionIdByName(String resolutionStr) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			for (NormalizerResolution normalizerResolution : lblobTyping.getNormalizer()) {
				if (normalizerResolution.getResolution().equals(resolutionStr)) {
					return normalizerResolution.getId();
				}
			}
		}
		return -1;
	}

	/**
	 * Provides the normalizer resolution for a specific normalizer resolution
	 * id.
	 * 
	 * @param resolutionId
	 * @return the normalizer resolution as {@link String} or ""(blank) if not
	 *         found
	 */
	@Override
	public String getNormalizerResolutionById(int resolutionId) {
		BlobTyping lblobTyping = getBlobTyping();
		if (lblobTyping != null) {
			for (NormalizerResolution normalizerResolution : lblobTyping.getNormalizer()) {
				if (resolutionId == normalizerResolution.getId()) {
					return normalizerResolution.getResolution();
				}
			}
		}
		return "";
	}

	private BlobTyping getBlobTyping() {
		if (blobTyping == null) {
			try {
				URL fileURL = new URL(DEFAULT_BLOBTYPING);
				InputStream inputStream = fileURL.openConnection().getInputStream();
				blobTyping = readBlob(inputStream);
			} catch (JAXBException | IOException e) {
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				String exceptionDetails = sw.toString();
				LOGGER.error("{}", exceptionDetails);
			}
		}
		return blobTyping;
	}
}
