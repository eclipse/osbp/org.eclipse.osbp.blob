/**
 *                                                                            

 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.blob.component;

import java.util.EventObject;

import org.eclipse.osbp.ui.api.customfields.IBlobEvent;

/**
 * Event object that provides all the required data to inform about a realized
 * upload and persistence into the database.
 * 
 * - {@code uploadSuccessful}: boolean that indicates if a blob was successfully
 * uploaded and persisted into the database.
 * 
 * - {@code uploadedBlobId}: the uuid of the successfully uploaded and persisted
 * blob into the database or {@code null} otherwise.
 * 
 * - {@code errorMessage}: error message that indicates the reason of an
 * unsuccessfully attempt of uploading and persisting a blob into the database.
 * 
 * @author dominguez
 * 
 */
public class BlobEvent extends EventObject implements IBlobEvent {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2935848211519428017L;

	private boolean uploadSuccessful;
	private String uploadedBlobId;
	private String errorMessage;
	private String uploadedFile;

	/**
	 * Instantiates a new blob event.
	 *
	 * @param source the source
	 * @param uploadSuccessful the upload successful
	 * @param uploadedBlobId the uploaded blob id
	 * @param errorMessage the error message
	 * @param uploadedFile the uploaded file
	 */
	public BlobEvent(Object source, boolean uploadSuccessful,
			String uploadedBlobId, String errorMessage, String uploadedFile) {
		super(source);
		this.uploadSuccessful = uploadSuccessful;
		this.uploadedBlobId = uploadedBlobId;
		this.errorMessage = errorMessage;
		this.uploadedFile = uploadedFile;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ui.api.customfields.IBlobEvent#isUploadSuccessful()
	 */
	@Override
	public boolean isUploadSuccessful() {
		return uploadSuccessful;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ui.api.customfields.IBlobEvent#getUploadedBlobId()
	 */
	@Override
	public String getUploadedBlobId() {
		return uploadedBlobId;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ui.api.customfields.IBlobEvent#getErrorMessage()
	 */
	@Override
	public String getErrorMessage() {
		return errorMessage;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ui.api.customfields.IBlobEvent#getUploadedFile()
	 */
	@Override
	public String getUploadedFile() {
		return uploadedFile;
	}
}
