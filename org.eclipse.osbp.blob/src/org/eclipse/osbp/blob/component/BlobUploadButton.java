/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.blob.component;

import java.util.List;

import com.vaadin.server.Resource;
import com.vaadin.ui.CustomComponent;
import com.wcs.wcslib.vaadin.widget.multifileupload.component.UploadComponent;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.MultiFileUpload;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadFinishedHandler;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadStateWindow;

@SuppressWarnings("serial")
public class BlobUploadButton extends CustomComponent {
	private MultiFileUpload singleUpload;

	public BlobUploadButton(UploadFinishedHandler handler) {
		UploadStateWindow uploadStateWindow = new UploadStateWindow();
		singleUpload = new MultiFileUpload(handler, uploadStateWindow, false);
		singleUpload.setSizeFull();
		setCompositionRoot(singleUpload);
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	void setUploadButtonCaption(String buttonCaption) {
		singleUpload.setUploadButtonCaptions(buttonCaption, "");
	}

	public void setUploadAcceptedMimeTypes(List<String> mimeTypes) {
		singleUpload.setAcceptedMimeTypes(mimeTypes);
	}

	public void setUploadButtonIcon(Resource uploadIcon) {
		if (uploadIcon != null)
			singleUpload.setUploadButtonIcon(uploadIcon);
	}
	
	public UploadComponent getUploadComponent() {
		return singleUpload.getSmartUpload().getUpload();
	}
	
	public void setUploadButtonId(String uploadButtonId) {
		singleUpload.getSmartUpload().setId(uploadButtonId);
	}
	
	@Override
	public void focus() {
		 singleUpload.getSmartUpload().focus();
	}
}
