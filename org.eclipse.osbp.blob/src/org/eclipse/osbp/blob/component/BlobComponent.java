/**
 *                                                                            
 * Copyright (c) 2011, 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */

package org.eclipse.osbp.blob.component;

import org.eclipse.osbp.ui.api.customfields.IBlobService;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
public class BlobComponent extends Label {

	public BlobComponent(IBlobService blobService, String value, int displayResolutionId) {
		super(blobService.getImage(value, displayResolutionId), ContentMode.HTML);
	}
	
	@Override
	public String getValue() {
		return "";
	}
	
	@Override
	public String getDescription() {
		return "";
	}

}
