package org.eclipse.osbp.blob.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import org.eclipse.osbp.blob.entities.BlobMapping;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "BLOB_DATA")
@DiscriminatorValue(value = "BLOB_DATA")
@SuppressWarnings("all")
public class Blob implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Id
  @Column(name = "ID")
  private String id = java.util.UUID.randomUUID().toString();
  
  @Column(name = "DATA")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Valid
  private byte[] data;
  
  @Column(name = "RESOLUTION_ID")
  private int resolutionId;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "BLOB_MAPPING_ID")
  private BlobMapping blobMapping;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final String id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the data property or <code>null</code> if not present.
   */
  public byte[] getData() {
    checkDisposed();
    return this.data;
  }
  
  /**
   * Sets the data property to this instance.
   */
  public void setData(final byte[] data) {
    checkDisposed();
    this.data = data;
  }
  
  /**
   * @return Returns the resolutionId property or <code>null</code> if not present.
   */
  public int getResolutionId() {
    checkDisposed();
    return this.resolutionId;
  }
  
  /**
   * Sets the resolutionId property to this instance.
   */
  public void setResolutionId(final int resolutionId) {
    checkDisposed();
    this.resolutionId = resolutionId;
  }
  
  /**
   * @return Returns the blobMapping property or <code>null</code> if not present.
   */
  public BlobMapping getBlobMapping() {
    checkDisposed();
    return this.blobMapping;
  }
  
  /**
   * Sets the blobMapping property to this instance.
   * Since the reference is a container reference, the opposite reference (BlobMapping.blobsRef)
   * of the blobMapping will be handled automatically and no further coding is required to keep them in sync.
   * See {@link BlobMapping#setBlobsRef(BlobMapping)}.
   */
  public void setBlobMapping(final BlobMapping blobMapping) {
    checkDisposed();
    if (this.blobMapping != null) {
      this.blobMapping.internalRemoveFromBlobsRef(this);
    }
    internalSetBlobMapping(blobMapping);
    if (this.blobMapping != null) {
      this.blobMapping.internalAddToBlobsRef(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetBlobMapping(final BlobMapping blobMapping) {
    this.blobMapping = blobMapping;
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Blob other = (Blob) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.id== null) ? 0 : this.id.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
