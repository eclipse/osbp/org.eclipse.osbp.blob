package org.eclipse.osbp.blob.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import org.eclipse.osbp.blob.entities.ContentType;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
@SuppressWarnings("all")
public class MimeType implements Serializable, IBean {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @XmlAttribute
  @Basic
  private String mimeVersion;
  
  @XmlAttribute
  @Basic
  private String contentTransferEncoding;
  
  @XmlElement(name = "contentType")
  @Basic
  @Embedded
  @ElementCollection
  private List<ContentType> contentTypeList;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    this.dirty = dirty;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the mimeVersion property or <code>null</code> if not present.
   */
  public String getMimeVersion() {
    checkDisposed();
    return this.mimeVersion;
  }
  
  /**
   * Sets the mimeVersion property to this instance.
   */
  public void setMimeVersion(final String mimeVersion) {
    checkDisposed();
    this.mimeVersion = mimeVersion;
  }
  
  /**
   * @return Returns the contentTransferEncoding property or <code>null</code> if not present.
   */
  public String getContentTransferEncoding() {
    checkDisposed();
    return this.contentTransferEncoding;
  }
  
  /**
   * Sets the contentTransferEncoding property to this instance.
   */
  public void setContentTransferEncoding(final String contentTransferEncoding) {
    checkDisposed();
    this.contentTransferEncoding = contentTransferEncoding;
  }
  
  /**
   * @return Returns an unmodifiable list of contentTypeList.
   */
  public List<ContentType> getContentTypeList() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetContentTypeList());
  }
  
  /**
   * Sets the given contentTypeList to the object. Currently contained contentTypeList instances will be removed.
   * 
   * @param contentTypeList the list of new instances
   */
  public void setContentTypeList(final List<ContentType> contentTypeList) {
    // remove the old contentType
    for(ContentType oldElement : new ArrayList<ContentType>(this.internalGetContentTypeList())){
      removeFromContentTypeList(oldElement);
    }
    
    // add the new contentType
    for(ContentType newElement : contentTypeList){
      addToContentTypeList(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>ContentType</code>s thereby lazy initializing it.
   */
  public List<ContentType> internalGetContentTypeList() {
    if (this.contentTypeList == null) {
      this.contentTypeList = new ArrayList<ContentType>();
    }
    return this.contentTypeList;
  }
  
  /**
   * Adds the given contentType to this object. <p>
   * 
   */
  public void addToContentTypeList(final ContentType contentType) {
    checkDisposed();
    if (!getContentTypeList().contains(contentType)){
      internalAddToContentTypeList(contentType);
    }
    
  }
  
  public void removeFromContentTypeList(final ContentType contentType) {
    checkDisposed();
    internalGetContentTypeList().remove(contentType);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToContentTypeList(final ContentType contentType) {
    if(contentType == null) {
    	return;
    }
    
    		internalGetContentTypeList().add(contentType);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromContentTypeList(final ContentType contentType) {
    internalGetContentTypeList().remove(contentType);
  }
}
