package org.eclipse.osbp.blob.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.osbp.blob.entities.Attributes;
import org.eclipse.osbp.blob.entities.MimeType;
import org.eclipse.osbp.blob.entities.NormalizerResolution;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
@SuppressWarnings("all")
public class BlobTyping implements Serializable, IBean {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @XmlElement
  @Basic
  @Valid
  private MimeType mimeType;
  
  @XmlElement
  @Basic
  @Embedded
  @Valid
  private Attributes attributes;
  
  @XmlElementWrapper(name = "normalizer")
  @XmlElement(name = "resolution")
  @Basic
  @Embedded
  @ElementCollection
  private List<NormalizerResolution> normalizer;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    this.dirty = dirty;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the mimeType property or <code>null</code> if not present.
   */
  public MimeType getMimeType() {
    checkDisposed();
    return this.mimeType;
  }
  
  /**
   * Sets the mimeType property to this instance.
   */
  public void setMimeType(final MimeType mimeType) {
    checkDisposed();
    this.mimeType = mimeType;
  }
  
  /**
   * @return Returns the attributes property or <code>null</code> if not present.
   */
  public Attributes getAttributes() {
    checkDisposed();
    return this.attributes;
  }
  
  /**
   * Sets the attributes property to this instance.
   */
  public void setAttributes(final Attributes attributes) {
    checkDisposed();
    this.attributes = attributes;
  }
  
  /**
   * @return Returns an unmodifiable list of normalizer.
   */
  public List<NormalizerResolution> getNormalizer() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetNormalizer());
  }
  
  /**
   * Sets the given normalizer to the object. Currently contained normalizer instances will be removed.
   * 
   * @param normalizer the list of new instances
   */
  public void setNormalizer(final List<NormalizerResolution> normalizer) {
    // remove the old normalizerResolution
    for(NormalizerResolution oldElement : new ArrayList<NormalizerResolution>(this.internalGetNormalizer())){
      removeFromNormalizer(oldElement);
    }
    
    // add the new normalizerResolution
    for(NormalizerResolution newElement : normalizer){
      addToNormalizer(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>NormalizerResolution</code>s thereby lazy initializing it.
   */
  public List<NormalizerResolution> internalGetNormalizer() {
    if (this.normalizer == null) {
      this.normalizer = new ArrayList<NormalizerResolution>();
    }
    return this.normalizer;
  }
  
  /**
   * Adds the given normalizerResolution to this object. <p>
   * 
   */
  public void addToNormalizer(final NormalizerResolution normalizerResolution) {
    checkDisposed();
    if (!getNormalizer().contains(normalizerResolution)){
      internalAddToNormalizer(normalizerResolution);
    }
    
  }
  
  public void removeFromNormalizer(final NormalizerResolution normalizerResolution) {
    checkDisposed();
    internalGetNormalizer().remove(normalizerResolution);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToNormalizer(final NormalizerResolution normalizerResolution) {
    if(normalizerResolution == null) {
    	return;
    }
    
    		internalGetNormalizer().add(normalizerResolution);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromNormalizer(final NormalizerResolution normalizerResolution) {
    internalGetNormalizer().remove(normalizerResolution);
  }
}
