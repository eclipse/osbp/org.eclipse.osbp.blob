package org.eclipse.osbp.blob.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import org.eclipse.osbp.dsl.common.datatypes.IBean;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@XmlAccessorType(XmlAccessType.FIELD)
@Embeddable
@SuppressWarnings("all")
public class Attributes implements Serializable, IBean {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @XmlElement
  @Basic
  private String size;
  
  @XmlElement
  @Basic
  private String resolution;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    this.dirty = dirty;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the size property or <code>null</code> if not present.
   */
  public String getSize() {
    checkDisposed();
    return this.size;
  }
  
  /**
   * Sets the size property to this instance.
   */
  public void setSize(final String size) {
    checkDisposed();
    this.size = size;
  }
  
  /**
   * @return Returns the resolution property or <code>null</code> if not present.
   */
  public String getResolution() {
    checkDisposed();
    return this.resolution;
  }
  
  /**
   * Sets the resolution property to this instance.
   */
  public void setResolution(final String resolution) {
    checkDisposed();
    this.resolution = resolution;
  }
}
