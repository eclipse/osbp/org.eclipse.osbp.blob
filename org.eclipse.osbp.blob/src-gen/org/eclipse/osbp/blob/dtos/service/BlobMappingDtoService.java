package org.eclipse.osbp.blob.dtos.service;

import org.eclipse.osbp.blob.dtos.BlobMappingDto;
import org.eclipse.osbp.blob.entities.BlobMapping;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;

@SuppressWarnings("all")
public class BlobMappingDtoService extends AbstractDTOService<BlobMappingDto, BlobMapping> {
  public BlobMappingDtoService() {
    // set the default persistence ID
    setPersistenceId("blob");
  }
  
  public Class<BlobMappingDto> getDtoClass() {
    return BlobMappingDto.class;
  }
  
  public Class<BlobMapping> getEntityClass() {
    return BlobMapping.class;
  }
  
  public Object getId(final BlobMappingDto dto) {
    return dto.getId();
  }
}
