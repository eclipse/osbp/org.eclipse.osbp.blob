package org.eclipse.osbp.blob.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.validation.Valid;
import org.eclipse.osbp.blob.dtos.AttributesDto;
import org.eclipse.osbp.blob.dtos.MimeTypeDto;
import org.eclipse.osbp.blob.dtos.NormalizerResolutionDto;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;

@SuppressWarnings("all")
public class BlobTypingDto implements IDto, Serializable, PropertyChangeListener {
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @Valid
  private MimeTypeDto mimeType;
  
  @DomainReference
  private AttributesDto attributes;
  
  @DomainReference
  private List<NormalizerResolutionDto> normalizer;
  
  public BlobTypingDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link BlobTyping} to the dto {@link BlobTypingDto}.
   * 
   */
  protected void installLazyCollections() {
    
  }
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
   */
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
   */
  public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    firePropertyChange("dirty", this.dirty, this.dirty = dirty );
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    firePropertyChange("disposed", this.disposed, this.disposed = true);
  }
  
  /**
   * Returns the mimeType property.
   */
  public MimeTypeDto getMimeType() {
    if(this.mimeType== null){
      this.mimeType = new MimeTypeDto();
    }
    return this.mimeType;
  }
  
  /**
   * Sets the <code>mimeType</code> property to this instance.
   * 
   * @param mimeType - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setMimeType(final MimeTypeDto mimeType) {
    // ensure that embedded beans will notify their parent about changes
    // so their dirty state can be handled properly
    if (this.mimeType != null) {
    	this.mimeType.removePropertyChangeListener(this);
    }
    
    firePropertyChange("mimeType", this.mimeType, this.mimeType = mimeType );
    
    if (this.mimeType != null) {
    	this.mimeType.addPropertyChangeListener(this);
    }
  }
  
  /**
   * Returns the attributes property.
   */
  public AttributesDto getAttributes() {
    if(this.attributes== null){
      this.attributes = new AttributesDto();this.attributes.addPropertyChangeListener(this);
    }
    return this.attributes;
  }
  
  /**
   * Sets the <code>attributes</code> property to this instance.
   * 
   * @param attributes - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setAttributes(final AttributesDto attributes) {
    checkDisposed();
    firePropertyChange("attributes", this.attributes, this.attributes = attributes);
  }
  
  /**
   * Returns an unmodifiable list of normalizer.
   */
  public List<NormalizerResolutionDto> getNormalizer() {
    return Collections.unmodifiableList(internalGetNormalizer());
  }
  
  /**
   * Returns the list of <code>NormalizerResolutionDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<NormalizerResolutionDto> internalGetNormalizer() {
    if (this.normalizer == null) {
      this.normalizer = new java.util.ArrayList<NormalizerResolutionDto>();
    }
    return this.normalizer;
  }
  
  /**
   * Adds the given normalizerResolutionDto to this object. <p>
   * 
   * @param normalizerResolutionDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToNormalizer(final NormalizerResolutionDto normalizerResolutionDto) {
    checkDisposed();
    
    internalAddToNormalizer(normalizerResolutionDto);
  }
  
  public void removeFromNormalizer(final NormalizerResolutionDto normalizerResolutionDto) {
    checkDisposed();
    
    internalRemoveFromNormalizer(normalizerResolutionDto);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToNormalizer(final NormalizerResolutionDto normalizerResolutionDto) {
    // add this as property change listener for embeddable beans
    normalizerResolutionDto.addPropertyChangeListener(this);
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<NormalizerResolutionDto> oldList = null;
    		if(internalGetNormalizer() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetNormalizer()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetNormalizer());
    		}
    		internalGetNormalizer().add(normalizerResolutionDto);
    		firePropertyChange("normalizer", oldList, internalGetNormalizer());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromNormalizer(final NormalizerResolutionDto normalizerResolutionDto) {
    // remove this as property change listener from the embeddable bean
    normalizerResolutionDto.removePropertyChangeListener(this);
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<NormalizerResolutionDto> oldList = null;
    	if(internalGetNormalizer() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetNormalizer()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetNormalizer());
    	}
    	internalGetNormalizer().remove(normalizerResolutionDto);
    	firePropertyChange("normalizer", oldList, internalGetNormalizer());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetNormalizer().remove(normalizerResolutionDto);
    }
  }
  
  /**
   * Sets the <code>normalizer</code> property to this instance.
   * 
   * @param normalizer - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setNormalizer(final List<NormalizerResolutionDto> normalizer) {
    checkDisposed();
    for (NormalizerResolutionDto dto : internalGetNormalizer().toArray(new NormalizerResolutionDto[this.normalizer.size()])) {
    	removeFromNormalizer(dto);
    }
    
    if(normalizer == null) {
    	return;
    }
    
    for (NormalizerResolutionDto dto : normalizer) {
    	addToNormalizer(dto);
    }
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    
    
    	if(source == mimeType){
    		firePropertyChange("mimeType" + "_" + event.getPropertyName(), event.getOldValue(), event.getNewValue());
    	} else 
    
    
    	if(source == attributes){
    		firePropertyChange("attributes" + "_" + event.getPropertyName(), event.getOldValue(), event.getNewValue());
    	} else 
    
    
    	if(source == normalizer){
    		firePropertyChange("normalizer" + "_" + event.getPropertyName(), event.getOldValue(), event.getNewValue());
    	} else 
    { 
    	// no super class available to forward event
    }
  }
}
