package org.eclipse.osbp.blob.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.blob.dtos.AttributesDto;
import org.eclipse.osbp.blob.dtos.BlobTypingDto;
import org.eclipse.osbp.blob.dtos.MimeTypeDto;
import org.eclipse.osbp.blob.dtos.NormalizerResolutionDto;
import org.eclipse.osbp.blob.entities.Attributes;
import org.eclipse.osbp.blob.entities.BlobTyping;
import org.eclipse.osbp.blob.entities.MimeType;
import org.eclipse.osbp.blob.entities.NormalizerResolution;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link BlobTypingDto} to and from the entity {@link BlobTyping}.
 * 
 */
@SuppressWarnings("all")
public class BlobTypingDtoMapper<DTO extends BlobTypingDto, ENTITY extends BlobTyping> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public BlobTyping createEntity() {
    return new BlobTyping();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public BlobTypingDto createDto() {
    return new BlobTypingDto();
  }
  
  /**
   * Maps the entity {@link BlobTyping} to the dto {@link BlobTypingDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final BlobTypingDto dto, final BlobTyping entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    dto.setMimeType(toDto_mimeType(entity, context));
    dto.setAttributes(toDto_attributes(entity, context));
  }
  
  /**
   * Maps the dto {@link BlobTypingDto} to the entity {@link BlobTyping}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final BlobTypingDto dto, final BlobTyping entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    
    entity.setMimeType(toEntity_mimeType(dto, entity, context));
    entity.setAttributes(toEntity_attributes(dto, entity, context));
    toEntity_normalizer(dto, entity, context);
  }
  
  /**
   * Maps the property mimeType from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected MimeTypeDto toDto_mimeType(final BlobTyping in, final MappingContext context) {
    if(in.getMimeType() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<MimeTypeDto, MimeType> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<MimeTypeDto, MimeType>) getToDtoMapper(MimeTypeDto.class, in.getMimeType().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	MimeTypeDto dto = null;
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getMimeType(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property mimeType from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected MimeType toEntity_mimeType(final BlobTypingDto in, final BlobTyping parentEntity, final MappingContext context) {
    if(in.getMimeType() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<MimeTypeDto, MimeType> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<MimeTypeDto, MimeType>) getToEntityMapper(in.getMimeType().getClass(), MimeType.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	MimeType entity = mapper.createEntity();
    	mapper.mapToEntity(in.getMimeType(), entity, context);
    	return entity;							
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property attributes from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected AttributesDto toDto_attributes(final BlobTyping in, final MappingContext context) {
    if(in.getAttributes() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AttributesDto, Attributes> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AttributesDto, Attributes>) getToDtoMapper(AttributesDto.class, in.getAttributes().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	AttributesDto dto = null;
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getAttributes(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property attributes from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected Attributes toEntity_attributes(final BlobTypingDto in, final BlobTyping parentEntity, final MappingContext context) {
    if(in.getAttributes() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<AttributesDto, Attributes> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<AttributesDto, Attributes>) getToEntityMapper(in.getAttributes().getClass(), Attributes.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	Attributes entity = null;
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getAttributes(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property normalizer from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<NormalizerResolutionDto> toDto_normalizer(final BlobTyping in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property normalizer from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<NormalizerResolution> toEntity_normalizer(final BlobTypingDto in, final BlobTyping parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<NormalizerResolutionDto, NormalizerResolution> mapper = getToEntityMapper(NormalizerResolutionDto.class, NormalizerResolution.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<NormalizerResolutionDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<NormalizerResolutionDto>) in.internalGetNormalizer();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToNormalizer,
    		parentEntity::internalRemoveFromNormalizer);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
  
  public String createEntityHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
}
