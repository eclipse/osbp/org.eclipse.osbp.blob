package org.eclipse.osbp.blob.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.blob.dtos.ContentTypeDto;
import org.eclipse.osbp.blob.dtos.MimeTypeDto;
import org.eclipse.osbp.blob.entities.ContentType;
import org.eclipse.osbp.blob.entities.MimeType;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link MimeTypeDto} to and from the entity {@link MimeType}.
 * 
 */
@SuppressWarnings("all")
public class MimeTypeDtoMapper<DTO extends MimeTypeDto, ENTITY extends MimeType> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public MimeType createEntity() {
    return new MimeType();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public MimeTypeDto createDto() {
    return new MimeTypeDto();
  }
  
  /**
   * Maps the entity {@link MimeType} to the dto {@link MimeTypeDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final MimeTypeDto dto, final MimeType entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    dto.setMimeVersion(toDto_mimeVersion(entity, context));
    dto.setContentTransferEncoding(toDto_contentTransferEncoding(entity, context));
  }
  
  /**
   * Maps the dto {@link MimeTypeDto} to the entity {@link MimeType}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final MimeTypeDto dto, final MimeType entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    
    entity.setMimeVersion(toEntity_mimeVersion(dto, entity, context));
    entity.setContentTransferEncoding(toEntity_contentTransferEncoding(dto, entity, context));
    toEntity_contentTypeList(dto, entity, context);
  }
  
  /**
   * Maps the property mimeVersion from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_mimeVersion(final MimeType in, final MappingContext context) {
    return in.getMimeVersion();
  }
  
  /**
   * Maps the property mimeVersion from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_mimeVersion(final MimeTypeDto in, final MimeType parentEntity, final MappingContext context) {
    return in.getMimeVersion();
  }
  
  /**
   * Maps the property contentTransferEncoding from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_contentTransferEncoding(final MimeType in, final MappingContext context) {
    return in.getContentTransferEncoding();
  }
  
  /**
   * Maps the property contentTransferEncoding from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_contentTransferEncoding(final MimeTypeDto in, final MimeType parentEntity, final MappingContext context) {
    return in.getContentTransferEncoding();
  }
  
  /**
   * Maps the property contentTypeList from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<ContentTypeDto> toDto_contentTypeList(final MimeType in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property contentTypeList from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<ContentType> toEntity_contentTypeList(final MimeTypeDto in, final MimeType parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<ContentTypeDto, ContentType> mapper = getToEntityMapper(ContentTypeDto.class, ContentType.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<ContentTypeDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<ContentTypeDto>) in.internalGetContentTypeList();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToContentTypeList,
    		parentEntity::internalRemoveFromContentTypeList);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
  
  public String createEntityHash(final Object in) {
    throw new UnsupportedOperationException("No id attribute available");
  }
}
