package org.eclipse.osbp.blob.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.blob.dtos.BlobDto;
import org.eclipse.osbp.blob.dtos.BlobMappingDto;
import org.eclipse.osbp.blob.entities.Blob;
import org.eclipse.osbp.blob.entities.BlobMapping;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link BlobMappingDto} to and from the entity {@link BlobMapping}.
 * 
 */
@SuppressWarnings("all")
public class BlobMappingDtoMapper<DTO extends BlobMappingDto, ENTITY extends BlobMapping> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public BlobMapping createEntity() {
    return new BlobMapping();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public BlobMappingDto createDto() {
    return new BlobMappingDto();
  }
  
  /**
   * Maps the entity {@link BlobMapping} to the dto {@link BlobMappingDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final BlobMappingDto dto, final BlobMapping entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    dto.setId(toDto_id(entity, context));
    dto.setUniqueName(toDto_uniqueName(entity, context));
    dto.setFileName(toDto_fileName(entity, context));
    dto.setMimeTypeId(toDto_mimeTypeId(entity, context));
  }
  
  /**
   * Maps the dto {@link BlobMappingDto} to the entity {@link BlobMapping}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final BlobMappingDto dto, final BlobMapping entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    
    entity.setId(toEntity_id(dto, entity, context));
    entity.setUniqueName(toEntity_uniqueName(dto, entity, context));
    entity.setFileName(toEntity_fileName(dto, entity, context));
    entity.setMimeTypeId(toEntity_mimeTypeId(dto, entity, context));
    toEntity_blobsRef(dto, entity, context);
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_id(final BlobMapping in, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_id(final BlobMappingDto in, final BlobMapping parentEntity, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property uniqueName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_uniqueName(final BlobMapping in, final MappingContext context) {
    return in.getUniqueName();
  }
  
  /**
   * Maps the property uniqueName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_uniqueName(final BlobMappingDto in, final BlobMapping parentEntity, final MappingContext context) {
    return in.getUniqueName();
  }
  
  /**
   * Maps the property fileName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_fileName(final BlobMapping in, final MappingContext context) {
    return in.getFileName();
  }
  
  /**
   * Maps the property fileName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_fileName(final BlobMappingDto in, final BlobMapping parentEntity, final MappingContext context) {
    return in.getFileName();
  }
  
  /**
   * Maps the property mimeTypeId from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_mimeTypeId(final BlobMapping in, final MappingContext context) {
    return in.getMimeTypeId();
  }
  
  /**
   * Maps the property mimeTypeId from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_mimeTypeId(final BlobMappingDto in, final BlobMapping parentEntity, final MappingContext context) {
    return in.getMimeTypeId();
  }
  
  /**
   * Maps the property blobsRef from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<BlobDto> toDto_blobsRef(final BlobMapping in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property blobsRef from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<Blob> toEntity_blobsRef(final BlobMappingDto in, final BlobMapping parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<BlobDto, Blob> mapper = getToEntityMapper(BlobDto.class, Blob.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<BlobDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<BlobDto>) in.internalGetBlobsRef();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToBlobsRef,
    		parentEntity::internalRemoveFromBlobsRef);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(BlobMappingDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(BlobMapping.class, in);
  }
}
