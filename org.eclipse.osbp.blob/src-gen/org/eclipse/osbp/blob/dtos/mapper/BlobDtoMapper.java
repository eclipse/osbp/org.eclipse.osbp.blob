package org.eclipse.osbp.blob.dtos.mapper;

import org.eclipse.osbp.blob.dtos.BlobDto;
import org.eclipse.osbp.blob.dtos.BlobMappingDto;
import org.eclipse.osbp.blob.entities.Blob;
import org.eclipse.osbp.blob.entities.BlobMapping;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link BlobDto} to and from the entity {@link Blob}.
 * 
 */
@SuppressWarnings("all")
public class BlobDtoMapper<DTO extends BlobDto, ENTITY extends Blob> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public Blob createEntity() {
    return new Blob();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public BlobDto createDto() {
    return new BlobDto();
  }
  
  /**
   * Maps the entity {@link Blob} to the dto {@link BlobDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final BlobDto dto, final Blob entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    dto.setId(toDto_id(entity, context));
    dto.setData(toDto_data(entity, context));
    dto.setResolutionId(toDto_resolutionId(entity, context));
    dto.setBlobMapping(toDto_blobMapping(entity, context));
  }
  
  /**
   * Maps the dto {@link BlobDto} to the entity {@link Blob}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final BlobDto dto, final Blob entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    
    entity.setId(toEntity_id(dto, entity, context));
    entity.setData(toEntity_data(dto, entity, context));
    entity.setResolutionId(toEntity_resolutionId(dto, entity, context));
    entity.setBlobMapping(toEntity_blobMapping(dto, entity, context));
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_id(final Blob in, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_id(final BlobDto in, final Blob parentEntity, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property data from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toDto_data(final Blob in, final MappingContext context) {
    return in.getData();
  }
  
  /**
   * Maps the property data from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toEntity_data(final BlobDto in, final Blob parentEntity, final MappingContext context) {
    return in.getData();
  }
  
  /**
   * Maps the property resolutionId from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_resolutionId(final Blob in, final MappingContext context) {
    return in.getResolutionId();
  }
  
  /**
   * Maps the property resolutionId from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_resolutionId(final BlobDto in, final Blob parentEntity, final MappingContext context) {
    return in.getResolutionId();
  }
  
  /**
   * Maps the property blobMapping from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected BlobMappingDto toDto_blobMapping(final Blob in, final MappingContext context) {
    if(in.getBlobMapping() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<BlobMappingDto, BlobMapping> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<BlobMappingDto, BlobMapping>) getToDtoMapper(BlobMappingDto.class, in.getBlobMapping().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	BlobMappingDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getBlobMapping()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getBlobMapping(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getBlobMapping(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property blobMapping from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected BlobMapping toEntity_blobMapping(final BlobDto in, final Blob parentEntity, final MappingContext context) {
    if(in.getBlobMapping() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<BlobMappingDto, BlobMapping> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<BlobMappingDto, BlobMapping>) getToEntityMapper(in.getBlobMapping().getClass(), BlobMapping.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	BlobMapping entity = null;
    	entity = context.get(mapper.createEntityHash(in.getBlobMapping()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (BlobMapping) context
    			.findEntityByEntityManager(BlobMapping.class, in.getBlobMapping().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getBlobMapping()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getBlobMapping(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(BlobDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Blob.class, in);
  }
}
