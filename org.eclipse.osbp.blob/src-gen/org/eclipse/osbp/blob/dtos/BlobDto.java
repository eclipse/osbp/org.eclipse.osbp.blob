package org.eclipse.osbp.blob.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.validation.Valid;
import org.eclipse.osbp.blob.dtos.BlobMappingDto;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Id;

@SuppressWarnings("all")
public class BlobDto implements IDto, Serializable, PropertyChangeListener {
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @Id
  private String id = java.util.UUID.randomUUID().toString();
  
  @Valid
  private byte[] data;
  
  private int resolutionId;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private BlobMappingDto blobMapping;
  
  public BlobDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link Blob} to the dto {@link BlobDto}.
   * 
   */
  protected void installLazyCollections() {
    
  }
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
   */
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
   */
  public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    firePropertyChange("dirty", this.dirty, this.dirty = dirty );
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    firePropertyChange("disposed", this.disposed, this.disposed = true);
  }
  
  /**
   * Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    return this.id;
  }
  
  /**
   * Sets the <code>id</code> property to this instance.
   * 
   * @param id - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setId(final String id) {
    firePropertyChange("id", this.id, this.id = id );
    				installLazyCollections();
  }
  
  /**
   * Returns the data property or <code>null</code> if not present.
   */
  public byte[] getData() {
    return this.data;
  }
  
  /**
   * Sets the <code>data</code> property to this instance.
   * 
   * @param data - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setData(final byte[] data) {
    firePropertyChange("data", this.data, this.data = data );
  }
  
  /**
   * Returns the resolutionId property or <code>null</code> if not present.
   */
  public int getResolutionId() {
    return this.resolutionId;
  }
  
  /**
   * Sets the <code>resolutionId</code> property to this instance.
   * 
   * @param resolutionId - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setResolutionId(final int resolutionId) {
    firePropertyChange("resolutionId", this.resolutionId, this.resolutionId = resolutionId );
  }
  
  /**
   * Returns the blobMapping property or <code>null</code> if not present.
   */
  public BlobMappingDto getBlobMapping() {
    return this.blobMapping;
  }
  
  /**
   * Sets the <code>blobMapping</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>BlobMappingDto#
   * blobsRef</code> of the <code>blobMapping</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link BlobMappingDto#setBlobsRef(BlobMappingDto)
   * 
   * @param blobMapping - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setBlobMapping(final BlobMappingDto blobMapping) {
    checkDisposed();
    if (this.blobMapping != null) {
    	this.blobMapping.internalRemoveFromBlobsRef(this);
    }
    
    internalSetBlobMapping(blobMapping);
    
    if (this.blobMapping != null) {
    	this.blobMapping.internalAddToBlobsRef(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetBlobMapping(final BlobMappingDto blobMapping) {
    firePropertyChange("blobMapping", this.blobMapping, this.blobMapping = blobMapping);
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BlobDto other = (BlobDto) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	// no super class available to forward event
    }
  }
}
